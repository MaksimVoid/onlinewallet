**This is repo for final project for js online course**

Project name - Online wallet.
Using this app you can record your expenses into categories and keep track of it .

For getting started just run *index.html* file in browser.

For authorization, you can use the following credentials:

| LOGIN | PASSWORD |
| ------ | ------ |
| admin | 123 |
| Nastya | qwerty1234 |
| Maksim | qwerty1234 |
| Yauhen | qwerty1234 |