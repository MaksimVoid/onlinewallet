const MAIN_URL = 'https://5f5913638040620016ab8d31.mockapi.io/owal';
const LOGIN_URL = '/login';

const EDIT_BTN = 'Edit';
const DELETE_BTN = 'Delete';
const CLOSE_BTN = 'Close';
const LOGIN_BTN = 'Login';
const LOG_IN_BTN = 'Log In';
const SAVE_BTN = 'Save';
const CANCEL_BTN = 'Cancel';
const ADD_BTN = 'Add';
const X_BTN = 'X';

const LOGIN_LABEL = 'Login';
const PASSWORD_LABEL = 'Password';
const NAME_LABEL = 'Name';
const SUM_LABEL = 'Sum';

const LOGIN_ACTION = 'login';
const CLOSE_ACTION = 'close';
const WATCH_ACTION = 'watch';
const ADD_ACTION = 'add';
const DELETE_ACTION = 'delete';
const SAVE_ACTION = 'save';
const CANCEL_ACTION = 'cancel';

const CURRENCY = 'UAH';