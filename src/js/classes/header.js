class Header {
    constructor(header, onLogout) {
        this._header = header;
        this._nickName = this._header.querySelector('.header__authorization_nick-name');
        this._header.addEventListener('click', ({target: {dataset: {action: ACTION}}}) => {
            if(ACTION === 'logout') {
                this.setNickName.call(this, '');
                onLogout.call(this);
            };
        });
    };

    setNickName = (newNickName) => {
        this._nickName.innerText = newNickName;
    };
};