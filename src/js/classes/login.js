const LOGIN_NAME = 'login';
const PASSWORD_NAME = 'password';
const MAX_LOGIN_LENGTH = 22;
const MAX_PASSWORD_LENGTH = 16;
const DEFAULT_ERROR_TEXT = 'Incorrect login or password';

class Login {
    constructor(form, errorText, mainUrl, onSubmit) {
        this._form = form;
        this._inputs = this._form.querySelectorAll('.form__fields_item input');
        this._errorText = errorText;
        this._mainUrl = mainUrl;

        this._form.addEventListener('submit', this._onFormSubmit.bind(this));
        this._form.addEventListener('input', this._onInput.bind(this));

        this._onSubmit = function(login) {
            this._setUser.call(this, login);
            onSubmit.call(this, login);
        };
    };

    _showError = (error) => {
        this._inputs.forEach(input => input.classList.add('form__input_error'));
        this._errorText.innerHTML = `<span>${error}</span>`;
    };

    _hideError() {
        this._inputs.forEach(input => input.classList.remove('form__input_error'));
        this._errorText.innerHTML = null;
    };

    _clearInputs() {
        this._inputs.forEach(input => input.value = '');
    };

    _setUser(login) {
        localStorage.setItem('userLogin', login);
    };

    async _login(login, password) {
        const response = await fetch(`${this._mainUrl}${LOGIN_URL}`);
        const data = await response.json();
        for(const { login: LOGIN, password: PASSWORD } of data) {
            if(LOGIN === login && PASSWORD === password) {
                return LOGIN;
            };
        };
        throw new Error(DEFAULT_ERROR_TEXT);
    };

    logout() {
        this._setUser('');
    };

    async _onFormSubmit(event) {
        event.preventDefault();
        const { login: { value: LOGIN }, password: { value: PASSWORD } } = this._form;
        try {
            const USER_LOGIN = await this._login(LOGIN, PASSWORD);
            this._hideError();
            this._clearInputs();
            this._setUser(USER_LOGIN);
            this._onSubmit(USER_LOGIN);
        } catch(error) {
            this._showError(error.message);
        };
    };

    _onInput({ target: { name: NAME, value: VALUE } }) {
        const IS_ERROR = (NAME === LOGIN_NAME && VALUE.length > MAX_LOGIN_LENGTH)
            || (NAME === PASSWORD_NAME && VALUE.length > MAX_PASSWORD_LENGTH);
        if(IS_ERROR) {
            this._showError(DEFAULT_ERROR_TEXT);
        } else {
            this._hideError();
        };
    };

    checkUser(onUnlogged, onLogged) {
        const USER_LOGIN = localStorage.getItem('userLogin');
        if(!USER_LOGIN) {
            onUnlogged();
        } else {
            onLogged(USER_LOGIN);
        }
    };
}