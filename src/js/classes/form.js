class Form {
    constructor(name) {
        this.name = name;
        this.form = this._createForm.call(this, this.name);
    };

    _createForm(name) {
        const form = document.createElement('form');
        form.name = name;
        form.classList.add('form');
        const fields = document.createElement('div');
        fields.classList.add('form__fields');
        form.append(fields);
        const actions = document.createElement('div');
        actions.classList.add('form__actions');
        form.append(actions);
        return form;
    };

    createField({ label: LABEL, type: TYPE, name: NAME, isRequired: IS_REQUIRED }) {
        const field = document.createElement('div');
        field.classList.add('form__fields_item');

        const label = document.createElement('label');
        label.htmlFor = NAME;
        label.innerText = `${LABEL}:`;
        field.append(label);

        const input = document.createElement('input');
        input.id = NAME;
        input.name = NAME;
        input.type = TYPE;
        input.classList.add('input');
        input.classList.add('form__input');
        input.required = !!IS_REQUIRED;
        field.append(input);

        this.form.querySelector('.form__fields').append(field);
    };

    createButton({ type: TYPE, text: TEXT, action: ACTION }) {
        const button = document.createElement('button');
        button.classList.add('button');
        button.type = TYPE;
        button.innerText = TEXT;
        button.dataset.action = ACTION;
        this.form.querySelector('.form__actions').append(button);
    };

    getForm() {
        return this.form;
    };
};

class SpecificForm extends Form {
    constructor(name) {
        super(name);
    };
    _createForm() {
        const content = document.createElement('div');
        content.classList.add('content');

        this.errorText = document.createElement('p');
        this.errorText.classList.add('form__error-text');
        content.append(this.errorText);

        this.form = super._createForm(this.name);
        content.append(this.form);

        return content;
    };

    showError(error) {
        this.errorText.innerText = error;
    };

    hideError() {
        this.errorText.innerText = '';
    };
};

class LoginForm extends SpecificForm {
    constructor(name) {
        super(name);
    };
    _createForm() {
        const content = super._createForm(this.name);
        super.createField({ label: LOGIN_LABEL, type: 'text', name: 'login', isRequired: true });
        super.createField({ label: PASSWORD_LABEL, type: 'password', name: 'password', isRequired: true });
        super.createButton({ type: 'submit', text: LOG_IN_BTN, action: LOGIN_ACTION });
        return content;
    };
};

class AddCategoryForm extends SpecificForm {
    constructor(name) {
        super(name);
    };
    _createForm() {
        const content = super._createForm(this.name);
        super.createField({ label: NAME_LABEL, type: 'text', name: 'name', isRequired: true });
        super.createButton({ type: 'button', text: CANCEL_BTN, action: CANCEL_ACTION });
        super.createButton({ type: 'button', text: SAVE_BTN, action: SAVE_ACTION });
        return content;
    };
};

class AddExpenseForm extends SpecificForm {
    constructor(name) {
        super(name);
    };
    _createForm() {
        const content = super._createForm(this.name);
        super.createField({ label: SUM_LABEL, type: 'text', name: 'sum', isRequired: true });
        super.createButton({ type: 'button', text: ADD_BTN, action: ADD_ACTION });
        return content;
    };
};