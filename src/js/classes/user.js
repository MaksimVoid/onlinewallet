const START_CATEGORY_ID = localStorage.getItem('categoryId') || 0;
const START_EXPENSE_ID = localStorage.getItem('expenseId') || 0;
const MAX_NAME_LENGTH = 30;
const CATEGORY_ID = 'category-';
const CATEGORY_BLOCK_ID = 'category-block-';
const EXPENSE_ID = 'expense-';

const getDate = () => {
    let dateObj = new Date();
    let month = String(dateObj.getMonth()).padStart(2, '0');
    let day = String(dateObj.getDate()).padStart(2, '0');
    let year = dateObj.getFullYear();
    let output = `${day}.${month}.${year}`;
    return output;
};

class User {
    constructor(login) {
        this.login = login;
        this.categories = this._fetchCategories.call(this, this.login) || [];
        this._nextCategoryId = START_CATEGORY_ID;
        this._nextExpenseId = START_EXPENSE_ID;
    };

    _setClasses(classes) {
        for(const CLASS of classes) {
            this.classList.add(CLASS);
        };
    };

    _saveCategories() {
        localStorage.setItem(this.login, JSON.stringify(this.categories));
    };

    _fetchCategories() {
        const data = localStorage.getItem(this.login);
        return data === 'undefined' ? [] : JSON.parse(data);
    };

    addCategory(name) {
        if(!name) {
            throw new Error('Set a name to the new category.');
        };
        if(name.length > MAX_NAME_LENGTH) {
            throw new Error(`The name must not exceed ${MAX_NAME_LENGTH} characters`);
        };
        this._nextCategoryId = (+this._nextCategoryId + 1).toString();
        localStorage.setItem('categoryId', this._nextCategoryId);
        const newCategory = {
            id: this._nextCategoryId,
            name,
            expenses: {
                list: [],
                total: 0
            }
        };
        this.categories.push(newCategory);
        this._saveCategories();
        return this._nextCategoryId;
    };

    deleteCategory(id) {
        this.categories = this.categories.filter(({ id: ID }) => ID !== id);
        this._saveCategories();
    };

    changeCategory(id, newCategory) {
        this.categories = this.categories.map(category => {
            const { id: ID } = category;
            if (ID === id) {
                return { ...category, ...newCategory, id };
            } else {
                return category;
            };
        });
        this._saveCategories();
    };

    getCategories() {
        return this.categories;
    };
};

class CategoriesSection extends User {
    constructor(login, section) {
        super(login);
        this.section = section;
        this.addBtn = this.section.querySelector('.categories__add_btn').parentNode;
        this.list = null;
        this.total = null;
        this._drawCategories.call(this);
    };

    _createBtn(text, action) {
        const btn = document.createElement('button');
        btn.classList.add('button');
        btn.innerText = text;
        btn.dataset.action = action;
        return btn;
    };

    _drawCategory(name, id) {
        const newCategory = document.createElement('div');
        newCategory.id = `${CATEGORY_ID}${id}`;
        newCategory.classList.add('categories__item');

        const mainBtn = document.createElement('button');
        super._setClasses.call(mainBtn, ['categories__item_main-btn', 'button', 'circle-btn']);
        mainBtn.innerText = name;
        mainBtn.dataset.action = WATCH_ACTION;
        newCategory.append(mainBtn);

        const deleteBtn = document.createElement('button');
        super._setClasses.call(deleteBtn, ['categories__item_delete-btn', 'button', 'circle-btn', 'delete-btn']);
        deleteBtn.innerText = X_BTN;
        deleteBtn.dataset.action = DELETE_ACTION;
        newCategory.append(deleteBtn);

        this.addBtn.before(newCategory);
    };

    _drawCategories() {
        this.categories.forEach(({ name: NAME, id: ID }) => {
            this._drawCategory(NAME, ID);
        });
    };

    addCategory(name) {
        const ID = super.addCategory(name);
        this._drawCategory(name, ID);
    };

    _wipeCategoryOff(id) {
        const deletedCategory = document.getElementById(`${CATEGORY_ID}${id}`);
        deletedCategory.remove();
    };

    deleteCategory(id) {
        this.categories = this.categories.filter(({ id: ID }) => {
            if(ID === id) {
                this._wipeCategoryOff(ID);
            }
            return ID !== id;
        });
        this._saveCategories();
    };

    _changeTotal(total) {
        this.total.innerHTML = `<p>Total: ${total} ${CURRENCY}</p>`;
    };

    _drawExpense({ sum: SUM, date: DATE, id: ID }) {
        const listItem = document.createElement('li');
        listItem.id = `${EXPENSE_ID}${ID}`;
        listItem.classList.add('category__list_item');
        listItem.innerHTML = `<span>${SUM} ${CURRENCY}</span><span>${DATE}</span>`;
        const XBtn = this._createBtn(X_BTN, 'delete');
        XBtn.classList.add('category__list_item_btn');
        XBtn.classList.add('delete-btn');
        listItem.append(XBtn);
        this.list.append(listItem);
    };

    addExpense(categoryId, sum) {
        if(!this.list) {
            throw new Error(`Create category block at first.`);
        };
        this.categories = this.categories.map(category => {
            const { id: ID } = category;
            if(categoryId === ID) {
                this._nextExpenseId = (+this._nextExpenseId + 1).toString();
                localStorage.setItem('expenseId', this._nextExpenseId);
                const newExpense = {
                    id: this._nextExpenseId,
                    sum,
                    date: getDate()
                }
                category.expenses.list.push(newExpense);
                category.expenses.total += +sum;
                this._drawExpense(newExpense);
                this._changeTotal(category.expenses.total);
            };
            return category;
        });
        this._saveCategories();
    };

    _wipeExpenseOff(id) {
        const deletedItem = document.getElementById(`${EXPENSE_ID}${id}`);
        deletedItem.remove();
    };

    deleteExpense(categoryId, expenseId) {
        this.categories = this.categories.map(category => {
            const { id: ID, expenses: { list } } = category;
            if(categoryId === ID) {
                category.expenses.list = list.filter(({ id: ITEM_ID, sum: SUM }) => {
                    if(ITEM_ID === expenseId) {
                        this._wipeExpenseOff(ITEM_ID);
                        category.expenses.total -= SUM;
                        this._changeTotal(category.expenses.total);
                    };
                    return ITEM_ID !== expenseId;
                });
            };
            return category;
        });
        this._saveCategories();
    };

    createCategoryBlock = (id, form) => {
        const { expenses: { total: TOTAL, list } } = this.categories.filter(({ id: ID }) => ID === id)[0];

        const block = document.createElement('div');
        block.id = `${CATEGORY_BLOCK_ID}${id}`;
        block.classList.add('category');

        const listBlock = document.createElement('div');
        listBlock.classList.add('category__block');
        block.append(listBlock);

        this.list = document.createElement('ul');
        this.list.classList.add('category__list');
        list.forEach(item => this._drawExpense(item));
        listBlock.append(this.list);

        this.total = document.createElement('div');
        this.total.classList.add('category__total');
        this._changeTotal(TOTAL);
        listBlock.append(this.total);

        form.classList.add('category__block');
        block.append(form);

        const buttonsBlock = document.createElement('div');
        buttonsBlock.classList.add('category__buttons');
        block.append(buttonsBlock);

        const closeBtn = this._createBtn(CLOSE_BTN, CLOSE_ACTION);
        buttonsBlock.append(closeBtn);

        return block;
    };
};