const SM_SIZE = 'sm';
const MD_SIZE = 'md';
const LG_SIZE = 'lg';

class Modal {
    constructor(size = SM_SIZE) {
        this.size = size;
        this.modal = null;
    };

    setTitle(newTitle) {
        this.title.innerText = newTitle;
    };

    _createHeader() {
        const header = document.createElement('div');
        header.classList.add('modal__header');
        this.title = document.createElement('span');
        this.title.innerText = this.title;
        this.title.classList.add('modal__header_title');
        header.append(this.title);
        return header;
    };

    _createMain(content) {
        const main = document.createElement('div');
        main.classList.add('modal__main');
        main.append(content);
        return main;
    };

    _createContent(main) {
        const fragment = document.createDocumentFragment();
        const header = this._createHeader();
        fragment.append(header);
        const mainContent = this._createMain(main);
        fragment.append(mainContent);
        return fragment;
    }

    _create(mainContent) {
        const modal = document.createElement('div');
        const modalBlock = document.createElement('div');
        modal.classList.add('modal');
        modalBlock.classList.add('modal__block');
        modalBlock.classList.add(`modal__block_${this.size}`);
        const modalContent = this._createContent(mainContent);
        modalBlock.append(modalContent);
        modal.prepend(modalBlock);
        return modal;
    };

    render(title, mainContent) {
        if(!this.modal) {
            this.modal = this._create(mainContent);
            const body = document.querySelector('body');
            body.append(this.modal);
        } else {
            const oldContent = this.modal.querySelector('.modal__main div');
            oldContent.replaceWith(mainContent);
            this.modal.style.display = 'block';
        };
        this.setTitle(title);
    };

    hide() {
        this.modal.style.display = 'none';
    };

    blur() {
        this.modal.classList.add('modal-blur');
    };
};