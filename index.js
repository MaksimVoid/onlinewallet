const NUM_REGEX = /^[0-9]+$/;

const getRootId = (id) => id.split('-').reverse()[0];

const getFormElements = (block) => {
    const form = block.querySelector('form');
    const errorText = block.querySelector('p');
    return { form, errorText };
};

const renderLoginModal = () => {
    loginModal.render('Log in', loginBlock);
    loginModal.blur();
};

const onAddCategoryFormCancel = () => {
    addCategoryForm.reset();
    addCategoryController.hideError();
    addCategoryModal.hide();
    addCategoryFormBlock.removeEventListener('click', onAddCategoryFormClick);
};

const onAddCategoryFormSave = () => {
    const { name: { value: VALUE } } = addCategoryForm;
    try {
        categoriesSection.addCategory(VALUE);
        addCategoryForm.reset();
        addCategoryModal.hide();
    } catch({message: ERROR}) {
        addCategoryController.showError(ERROR);
    }
};

const onAddCategoryFormClick = (event) => {
    event.stopPropagation();
    const { target: { dataset: { action: ACTION } } } = event;
    if(ACTION === CANCEL_ACTION) {
        onAddCategoryFormCancel();
    } else if(ACTION === SAVE_ACTION) {
        onAddCategoryFormSave();
    };
};

const onCategoryBlockClose = ({ target, currentTarget }) => {
    const form = target.closest('.category').querySelector('form');
    form.reset();
    addExpenseForm.hideError();
    categoryModal.hide();
    currentTarget.removeEventListener('click', onCategoryBlockClick);
};

const onAddExpense = ({ target, currentTarget }) => {
    const form = target.closest('.category').querySelector('form');
    const { elements: { sum: { value: NEW_SUM } } } = form;
    const { id: CATEGORY_ID } = currentTarget;
    const ROOT_CATEGORY_ID = getRootId(CATEGORY_ID);
    if(NUM_REGEX.test(NEW_SUM)) {
        categoriesSection.addExpense(ROOT_CATEGORY_ID, NEW_SUM);
        form.reset();
        addExpenseForm.hideError();
    } else {
        addExpenseForm.showError('Invalid value');
    };
};

const onDeleteExpense = ({ target, currentTarget }) => {
    const { parentNode: { id: EXPENSE_ID } } = target;
    const ROOT_EXPENSE_ID = getRootId(EXPENSE_ID);
    const { id: CATEGORY_ID } = currentTarget;
    const ROOT_CATEGORY_ID = getRootId(CATEGORY_ID);
    categoriesSection.deleteExpense(ROOT_CATEGORY_ID, ROOT_EXPENSE_ID);
};

const onCategoryBlockClick = (event) => {
    event.preventDefault();
    const { target } = event;
    const { dataset: { action: ACTION } } = target;
    if(ACTION === CLOSE_ACTION) {
        onCategoryBlockClose(event);
    } else if(ACTION === ADD_ACTION) {
        onAddExpense(event);
    } else if(ACTION === DELETE_ACTION) {
        onDeleteExpense(event);
    };
};

const onAddCategory = () => {
    addCategoryModal.render('Add new category', addCategoryFormBlock);
    addCategoryFormBlock.addEventListener('click', onAddCategoryFormClick);
};

const onOpenCategory = ({ target }) => {
    const { innerText: NAME, parentNode: { id: CATEGORY_ID } } = target;
    const ROOT_CATEGORY_ID = getRootId(CATEGORY_ID);
    const categoryBlock = categoriesSection.createCategoryBlock(ROOT_CATEGORY_ID, addExpenseForm.getForm());
    categoryBlock.addEventListener('click', onCategoryBlockClick);
    categoryModal.render(NAME, categoryBlock);
};

const onCategoriesBlockClick = (event) => {
    const { target } = event;
    const { dataset: { action: ACTION }, parentNode: { id: CATEGORY_ID } } = target;
    const ROOT_CATEGORY_ID = getRootId(CATEGORY_ID);
    if(ACTION === ADD_ACTION) {
        onAddCategory();
    } else if(ACTION === WATCH_ACTION) {
        onOpenCategory(event);
    } else if(ACTION === DELETE_ACTION) {
        categoriesSection.deleteCategory(ROOT_CATEGORY_ID);
    };
};

const onLoginSuccess = (login) => {
    header.setNickName(login);
    loginModal.hide();
    categoriesSection = new CategoriesSection(login, categoriesBlock);
    document.location.reload();
};

const onLogout = () => {
    login.logout();
    renderLoginModal();
};

const categoriesBlock = document.querySelector('.categories');

const USER_LOGIN = localStorage.getItem('userLogin');
let categoriesSection = USER_LOGIN ? new CategoriesSection(USER_LOGIN, categoriesBlock) : null;

const header = new Header(document.querySelector('.header'), onLogout);

const loginBlock = new LoginForm('login_form').getForm();
const { form: loginForm, errorText: loginErrorText } = getFormElements(loginBlock);
const login = new Login(loginForm, loginErrorText, MAIN_URL, onLoginSuccess);
const loginModal = new Modal('sm');
login.checkUser(renderLoginModal, header.setNickName);

const addCategoryModal = new Modal('sm');
const addCategoryController = new AddCategoryForm('add_category_form');
const addCategoryFormBlock = addCategoryController.getForm()
const { form: addCategoryForm } = getFormElements(addCategoryFormBlock);

const categoryModal = new Modal('lg');
const addExpenseForm = new AddExpenseForm('add_expense_form');

addCategoryForm.addEventListener('click', onAddCategoryFormClick);
categoriesBlock.addEventListener('click', onCategoriesBlockClick);

const removeListeners = () => {
    addCategoryForm.removeEventListener('click', onAddCategoryFormClick);
    categoriesBlock.removeEventListener('click', onCategoriesBlockClick);
    window.removeEventListener('beforeunload ', removeListeners);
};

window.addEventListener('beforeunload ', removeListeners);